import org.junit.*;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;
import models.*;

public class ApplicationTest extends FunctionalTest {

    @Test
    public void testThatIndexPageWorks() {
        Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testAdminSecurity() {
        Response response = GET("/admin");
        assertEquals(2 * 1, 2);
        assertStatus(302, response);
        assertHeaderEquals("Location", "/login", response);
    }
    
}